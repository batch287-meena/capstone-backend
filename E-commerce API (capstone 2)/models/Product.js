const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']	
	},
	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
			// The "new Date()" expression instantiates a new "date" that store the current date and time whenever a course is created in our database
		default: new Date()
	},
	userOrders: [{
		userID: {
			type: Object,
			required: [true, 'User ID is required.']
		},
		orderID: {
			type: Object,
			required: [true, 'Order ID is required.']
		}
	}]
})

module.exports = mongoose.model("Course", courseSchema);
