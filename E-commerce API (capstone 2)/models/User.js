const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [ true, "Email is required." ]
	},
	password: {
		type: String,
		required: [ true, "Password is required." ]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [{
		products: [{
			productID: {
				type: Object,
				required: [true, 'Product ID is required.']
			},
			productName: {
				type: String,
				required: [true, 'Product Name is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Please fill the quantity required.']
			}
		}],
		totalAmount: {
			type: Number,
			required: [true, 'Please fill the total anount required.']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model("User", userSchema);