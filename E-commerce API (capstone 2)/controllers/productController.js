const Product = require("../models/Product");
const auth = require("../auth");
const User = require("../models/User");

// To add a new product
module.exports.addProduct = (data) => {

	console.log(data);

	if(data.isAdmin){

		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((course, error) => {

			if(error){

				return "Error!";
			} else{

				return "Product is added successfully";
			}
		})
	}

	let message = Promise.resolve("User must be Admin to access this!");
	return message.then((value) => {
		return value
	});
};


// To get all the products
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	});
};


// To get product with a specific product ID
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(
		result => {
			return result;
		});
};


// To update a particular product with given product ID ...only Admin
module.exports.updateProduct = (reqParams, reqBody) => {

	if(data.isAdmin){

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if(error){
				return false;
			} else {
				return "Product is updated successfully!";
			};
		});
	};

	let message = Promise.resolve("User must be an Admin to access this.");
	return message.then((value) => {
		return value;
	});
};


// To archive a course from given productId..Only Admin
module.exports.archiveProduct = (reqParams, reqbody) => {

	if(reqbody.isAdmin){

		let updateActiveField = {
			isActive: false
		}

		return Product.findByIdAndUpdate( reqParams.productId, updateActiveField).then((product, err) => {

				if(err){

					return false;
				} else{

					return "Product archived successfully!"
				};
		});
	};

};
