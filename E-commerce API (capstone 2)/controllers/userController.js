const User = require("../models/User");
const Course = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// For registering user

module.exports.registerUser = (reqBody) => {
	letUser = new User ({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
		// password = #### #### ##
	})

	return newUser.save().then((user, err) => {

		if(err){
			return false;
		} else{
			return "You are successfully registered!"
		};
	});
}


// For user login

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {
		console.log(result);

		if(result == null){
			return "Please register before login."
		} else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else{
				return "Incorrect password!"
			};
		};
	});
};


// To retrieve user details
module.exports.getDetails = (reqParams) => {
	return User.findOne({ userId: reqParams }).then(result => {

		result.password = "";
		return result;
	});
};


// To make an user as an Admin
module.exports.makeAdmin = (data) => {
	console.log(data);
	if(data.isAdmin){
		let userToAdmin = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(data.userId, userToAdmin).then((user, err) => {

			if(err){
				return "Error!";
			} else{
				return "You are now an Admin!"
			};
		});
	};
};