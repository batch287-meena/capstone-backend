const express = require("express");
const mongoose = require("mongoose");

const userRoutes = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoute.js");


const cors = require("cors");

// Create an "app" variable that stores the result of the express function
const app = express();

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.tkxmolv.mongodb.net/capstone2", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

mongoose.connection.once("open", () => console.log('Now connected in the cloud.'))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});

app.use("/users", userRoutes);
app.use("/products", productRoutes);
api.use("/orders", orderRoutes);