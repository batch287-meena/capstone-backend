const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for creating a product
router.post("/", auth.verify, (req, res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the products
router.get("/products", (req, res) => {

    productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a specific product
router.get("/products/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a product
router.put("/products/:productId", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for archiving a course
router.put("/products/:productId/archive", auth.verify, (req, res) => {

	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});


module.exports = router;