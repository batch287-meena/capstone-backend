const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");


// Route to make an order for Non-admin only

router.post("/newOrder", auth.verify, (req,res) => {

	let data = {

		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	orderController.makeOrder(data).then(resultFromController => res.send(resultFromController));
});


// route to retrieve all the orders (Admin only)

router.get("/allOrders", auth.verify, (req,res) => {

	let adminDetail = auth.decode(req.headers.authorization).isAdmin

	orderController.getAllOrders(adminDetail).then( resultFromController => res.send(resultFromController));
});


// route to retrieve all the orders placed by a particular user

router.get("/myOrders", auth.verify, (req,res) => {

	orderController.getMyOrders(req.body.userId).then(resultFromController => res.send(resultFromController));
});


module.exports = router;